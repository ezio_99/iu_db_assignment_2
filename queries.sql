-- Number of payments that are smaller
EXPLAIN ANALYZE
SELECT *,
       (SELECT count(*)
        FROM rental r2,
             payment p2
        WHERE r2.rental_id
            = p2.rental_id
          AND p2.amount < p.amount) AS count_smaller_pay
FROM rental r,
     payment p
WHERE r.rental_id = p.rental_id;

-- Example of INSERT
EXPLAIN ANALYZE
INSERT INTO film(title, language_id, rental_duration, rental_rate, replacement_cost)
VALUES ('INNOPOLIS', 1, 5, 2.99, 15.99);

-- Example of UPDATE
EXPLAIN ANALYZE
UPDATE film
SET title = 'Innopolis'
WHERE title = 'INNOPOLIS';

-- Example of DELETE
EXPLAIN ANALYZE
DELETE
FROM film
WHERE title = 'Innopolis';
