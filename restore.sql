-- File paths need to be edited. Search for /db/data and
-- replace it with the path to the directory containing
-- the extracted data files.

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET client_min_messages = warning;
SET row_security = off;


ALTER DATABASE dvdrental OWNER TO root;

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
-- SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

IMPORT TABLE public.customer
(
    customer_id serial                                                  NOT NULL,
    store_id    smallint                                                NOT NULL,
    first_name  character varying(45)                                   NOT NULL,
    last_name   character varying(45)                                   NOT NULL,
    email       character varying(50),
    address_id  smallint                                                NOT NULL,
    activebool  boolean                     DEFAULT true                NOT NULL,
    create_date date                        DEFAULT ('now'::text)::date NOT NULL,
    last_update timestamp without time zone DEFAULT now(),
    active      integer
) CSV DATA
(
    'nodelocal://self/db/data/3055.dat'
) WITH DELIMITER = e'\t', nullif = '\N';


ALTER TABLE public.customer
    OWNER TO root;

IMPORT TABLE public.actor
(
    actor_id    serial                                    NOT NULL,
    first_name  character varying(45)                     NOT NULL,
    last_name   character varying(45)                     NOT NULL,
    last_update timestamp without time zone DEFAULT now() NOT NULL
) CSV DATA
(
    'nodelocal://self/db/data/3057.dat'
) WITH DELIMITER = e'\t', nullif = '\N';

ALTER TABLE public.actor
    OWNER TO root;

IMPORT TABLE public.category
(
    category_id serial                                    NOT NULL,
    name        character varying(25)                     NOT NULL,
    last_update timestamp without time zone DEFAULT now() NOT NULL
) CSV DATA
(
    'nodelocal://self/db/data/3059.dat'
) WITH DELIMITER = e'\t', nullif = '\N';

ALTER TABLE public.category
    OWNER TO root;

IMPORT TABLE public.film
(
    film_id          serial                      NOT NULL,
    title            character varying(255)      NOT NULL,
    description      text,
    release_year     int,
    language_id      smallint                    NOT NULL,
    rental_duration  smallint      DEFAULT 3     NOT NULL,
    rental_rate      numeric(4, 2) DEFAULT 4.99  NOT NULL,
    length           smallint,
    replacement_cost numeric(5, 2) DEFAULT 19.99 NOT NULL,
    rating           varchar(5)    DEFAULT 'G',
    last_update      timestamp     DEFAULT now() NOT NULL,
    special_features text[],
    fulltext text

    CONSTRAINT rating_right_domain CHECK (rating IN ('G', 'PG', 'PG-13', 'R', 'NC-17')),
    CONSTRAINT release_year_right_domain CHECK (release_year BETWEEN 1901 AND 2155)
) CSV DATA
(
    'nodelocal://self/db/data/3061_1.dat'
) WITH DELIMITER = e'\t', nullif = '\N';


ALTER TABLE public.film
    OWNER TO root;

IMPORT TABLE public.film_actor
(
    actor_id    smallint                                  NOT NULL,
    film_id     smallint                                  NOT NULL,
    last_update timestamp without time zone DEFAULT now() NOT NULL
) CSV DATA
(
    'nodelocal://self/db/data/3062.dat'
) WITH DELIMITER = e'\t', nullif = '\N';


ALTER TABLE public.film_actor
    OWNER TO root;

IMPORT TABLE public.film_category
(
    film_id     smallint                                  NOT NULL,
    category_id smallint                                  NOT NULL,
    last_update timestamp without time zone DEFAULT now() NOT NULL
) CSV DATA
(
    'nodelocal://self/db/data/3063.dat'
) WITH DELIMITER = e'\t', nullif = '\N';


ALTER TABLE public.film_category
    OWNER TO root;


IMPORT TABLE public.address
(
    address_id  serial                                    NOT NULL,
    address     character varying(50)                     NOT NULL,
    address2    character varying(50),
    district    character varying(20)                     NOT NULL,
    city_id     smallint                                  NOT NULL,
    postal_code character varying(10),
    phone       character varying(20)                     NOT NULL,
    last_update timestamp without time zone DEFAULT now() NOT NULL
) CSV DATA
(
    'nodelocal://self/db/data/3065.dat'
) WITH DELIMITER = e'\t', nullif = '\N';


ALTER TABLE public.address
    OWNER TO root;

IMPORT TABLE public.city
(
    city_id     serial                                    NOT NULL,
    city        character varying(50)                     NOT NULL,
    country_id  smallint                                  NOT NULL,
    last_update timestamp without time zone DEFAULT now() NOT NULL
) CSV DATA
(
    'nodelocal://self/db/data/3067.dat'
) WITH DELIMITER = e'\t', nullif = '\N';


ALTER TABLE public.city
    OWNER TO root;

IMPORT TABLE public.country
(
    country_id  serial                                    NOT NULL,
    country     character varying(50)                     NOT NULL,
    last_update timestamp without time zone DEFAULT now() NOT NULL
) CSV DATA
(
    'nodelocal://self/db/data/3069.dat'
) WITH DELIMITER = e'\t', nullif = '\N';


ALTER TABLE public.country
    OWNER TO root;

IMPORT TABLE public.inventory
(
    inventory_id serial                                    NOT NULL,
    film_id      smallint                                  NOT NULL,
    store_id     smallint                                  NOT NULL,
    last_update  timestamp without time zone DEFAULT now() NOT NULL
) CSV DATA
(
    'nodelocal://self/db/data/3071.dat'
) WITH DELIMITER = e'\t', nullif = '\N';


ALTER TABLE public.inventory
    OWNER TO root;

IMPORT TABLE public.language
(
    language_id serial                                    NOT NULL,
    name        character(20)                             NOT NULL,
    last_update timestamp without time zone DEFAULT now() NOT NULL
) CSV DATA
(
    'nodelocal://self/db/data/3073.dat'
) WITH DELIMITER = e'\t', nullif = '\N';


ALTER TABLE public.language
    OWNER TO root;

IMPORT TABLE public.payment
(
    payment_id   serial                      NOT NULL,
    customer_id  smallint                    NOT NULL,
    staff_id     smallint                    NOT NULL,
    rental_id    integer                     NOT NULL,
    amount       numeric(5, 2)               NOT NULL,
    payment_date timestamp without time zone NOT NULL
) CSV DATA
(
    'nodelocal://self/db/data/3075.dat'
) WITH DELIMITER = e'\t', nullif = '\N';


ALTER TABLE public.payment
    OWNER TO root;

IMPORT TABLE public.rental
(
    rental_id    serial                                    NOT NULL,
    rental_date  timestamp without time zone               NOT NULL,
    inventory_id integer                                   NOT NULL,
    customer_id  smallint                                  NOT NULL,
    return_date  timestamp without time zone,
    staff_id     smallint                                  NOT NULL,
    last_update  timestamp without time zone DEFAULT now() NOT NULL
) CSV DATA
(
    'nodelocal://self/db/data/3077.dat'
) WITH DELIMITER = e'\t', nullif = '\N';


ALTER TABLE public.rental
    OWNER TO root;

IMPORT TABLE public.staff
(
    staff_id    serial                                    NOT NULL,
    first_name  character varying(45)                     NOT NULL,
    last_name   character varying(45)                     NOT NULL,
    address_id  smallint                                  NOT NULL,
    email       character varying(50),
    store_id    smallint                                  NOT NULL,
    active      boolean                     DEFAULT true  NOT NULL,
    username    character varying(16)                     NOT NULL,
    password    character varying(40),
    last_update timestamp without time zone DEFAULT now() NOT NULL,
    picture     bytea
) CSV DATA
(
    'nodelocal://self/db/data/3079.dat'
) WITH DELIMITER = e'\t', nullif = '\N';

ALTER TABLE public.staff
    OWNER TO root;

IMPORT TABLE public.store
(
    store_id         serial                                    NOT NULL,
    manager_staff_id smallint                                  NOT NULL,
    address_id       smallint                                  NOT NULL,
    last_update      timestamp without time zone DEFAULT now() NOT NULL
) CSV DATA
(
    'nodelocal://self/db/data/3081.dat'
) WITH DELIMITER = e'\t', nullif = '\N';


ALTER TABLE public.store
    OWNER TO root;

-------------------------------------------------------------------------------------------------------------

ALTER TABLE ONLY public.actor
    ADD CONSTRAINT actor_pkey PRIMARY KEY (actor_id);

ALTER TABLE ONLY public.address
    ADD CONSTRAINT address_pkey PRIMARY KEY (address_id);

ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (category_id);

ALTER TABLE ONLY public.city
    ADD CONSTRAINT city_pkey PRIMARY KEY (city_id);

ALTER TABLE ONLY public.country
    ADD CONSTRAINT country_pkey PRIMARY KEY (country_id);

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (customer_id);

ALTER TABLE ONLY public.film_actor
    ADD CONSTRAINT film_actor_pkey PRIMARY KEY (actor_id, film_id);

ALTER TABLE ONLY public.film_category
    ADD CONSTRAINT film_category_pkey PRIMARY KEY (film_id, category_id);

ALTER TABLE ONLY public.film
    ADD CONSTRAINT film_pkey PRIMARY KEY (film_id);

ALTER TABLE ONLY public.inventory
    ADD CONSTRAINT inventory_pkey PRIMARY KEY (inventory_id);

ALTER TABLE ONLY public.language
    ADD CONSTRAINT language_pkey PRIMARY KEY (language_id);

ALTER TABLE ONLY public.payment
    ADD CONSTRAINT payment_pkey PRIMARY KEY (payment_id);

ALTER TABLE ONLY public.rental
    ADD CONSTRAINT rental_pkey PRIMARY KEY (rental_id);

ALTER TABLE ONLY public.staff
    ADD CONSTRAINT staff_pkey PRIMARY KEY (staff_id);

ALTER TABLE ONLY public.store
    ADD CONSTRAINT store_pkey PRIMARY KEY (store_id);

----------------------------------------------------------------------------------------------

-- CREATE INDEX film_fulltext_idx ON public.film USING gist (fulltext);
    
CREATE INDEX idx_actor_last_name ON public.actor USING btree (last_name);

CREATE INDEX idx_fk_address_id ON public.customer USING btree (address_id);

CREATE INDEX idx_fk_city_id ON public.address USING btree (city_id);

CREATE INDEX idx_fk_country_id ON public.city USING btree (country_id);

CREATE INDEX idx_fk_customer_id ON public.payment USING btree (customer_id);

CREATE INDEX idx_fk_film_id ON public.film_actor USING btree (film_id);

CREATE INDEX idx_fk_inventory_id ON public.rental USING btree (inventory_id);

CREATE INDEX idx_fk_language_id ON public.film USING btree (language_id);

CREATE INDEX idx_fk_rental_id ON public.payment USING btree (rental_id);

CREATE INDEX idx_fk_staff_id ON public.payment USING btree (staff_id);

CREATE INDEX idx_fk_store_id ON public.customer USING btree (store_id);

CREATE INDEX idx_last_name ON public.customer USING btree (last_name);

CREATE INDEX idx_store_id_film_id ON public.inventory USING btree (store_id, film_id);

CREATE INDEX idx_title ON public.film USING btree (title);

CREATE UNIQUE INDEX idx_unq_manager_staff_id ON public.store USING btree (manager_staff_id);

CREATE UNIQUE INDEX idx_unq_rental_rental_date_inventory_id_customer_id ON public.rental USING btree (rental_date, inventory_id, customer_id);

ALTER TABLE ONLY public.customer
    ADD CONSTRAINT customer_address_id_fkey FOREIGN KEY (address_id) REFERENCES public.address (address_id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY public.film_actor
    ADD CONSTRAINT film_actor_actor_id_fkey FOREIGN KEY (actor_id) REFERENCES public.actor (actor_id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY public.film_actor
    ADD CONSTRAINT film_actor_film_id_fkey FOREIGN KEY (film_id) REFERENCES public.film (film_id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY public.film_category
    ADD CONSTRAINT film_category_category_id_fkey FOREIGN KEY (category_id) REFERENCES public.category (category_id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY public.film_category
    ADD CONSTRAINT film_category_film_id_fkey FOREIGN KEY (film_id) REFERENCES public.film (film_id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY public.film
    ADD CONSTRAINT film_language_id_fkey FOREIGN KEY (language_id) REFERENCES public.language (language_id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY public.address
    ADD CONSTRAINT fk_address_city FOREIGN KEY (city_id) REFERENCES public.city (city_id);

ALTER TABLE ONLY public.city
    ADD CONSTRAINT fk_city FOREIGN KEY (country_id) REFERENCES public.country (country_id);

ALTER TABLE ONLY public.inventory
    ADD CONSTRAINT inventory_film_id_fkey FOREIGN KEY (film_id) REFERENCES public.film (film_id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY public.payment
    ADD CONSTRAINT payment_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES public.customer (customer_id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY public.payment
    ADD CONSTRAINT payment_rental_id_fkey FOREIGN KEY (rental_id) REFERENCES public.rental (rental_id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY public.payment
    ADD CONSTRAINT payment_staff_id_fkey FOREIGN KEY (staff_id) REFERENCES public.staff (staff_id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY public.rental
    ADD CONSTRAINT rental_customer_id_fkey FOREIGN KEY (customer_id) REFERENCES public.customer (customer_id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY public.rental
    ADD CONSTRAINT rental_inventory_id_fkey FOREIGN KEY (inventory_id) REFERENCES public.inventory (inventory_id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY public.rental
    ADD CONSTRAINT rental_staff_id_key FOREIGN KEY (staff_id) REFERENCES public.staff (staff_id);

ALTER TABLE ONLY public.staff
    ADD CONSTRAINT staff_address_id_fkey FOREIGN KEY (address_id) REFERENCES public.address (address_id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY public.store
    ADD CONSTRAINT store_address_id_fkey FOREIGN KEY (address_id) REFERENCES public.address (address_id) ON UPDATE CASCADE ON DELETE RESTRICT;

ALTER TABLE ONLY public.store
    ADD CONSTRAINT store_manager_staff_id_fkey FOREIGN KEY (manager_staff_id) REFERENCES public.staff (staff_id) ON UPDATE CASCADE ON DELETE RESTRICT;
