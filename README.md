# Init environment
    ./create_certs
    docker-compose up

    # after that in another terminal
    ./init_cluster

# Migration
    ./migrate
