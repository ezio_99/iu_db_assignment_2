COLUMN = -2
FILE = "data/3061.dat"
DELIMETER = "\t"
RESULT_FILE = "data/3061_1.dat"

with open(FILE, "r", encoding="utf-8") as input_, \
        open(RESULT_FILE, "w", encoding="utf-8") as output:
    for line in input_:
        if stripped := line.strip():
            fields = stripped.split(DELIMETER)
            s = "ARRAY["
            data = []
            for i in fields[COLUMN][1:-1].split(','):
                j = i.strip()
                if j.startswith('"') and j.endswith('"'):
                    data.append(f"'{j[1:-1]}'")
                else:
                    data.append(f"'{j}'")
            s += ",".join(data)
            s += "]"
            fields[COLUMN] = s
            output.write("\t".join(fields) + "\n")
